﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace SeleniumFirst
{
    class YaSchedulePageObject
    {
        public YaSchedulePageObject()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        // поле откуда
        [FindsBy(How = How.Name, Using = "fromName")]
        public IWebElement FromField { get; set; }

        // поле куда
        [FindsBy(How = How.Name, Using = "toName")]
        public IWebElement ToField { get; set; }

        //поле когда
        [FindsBy(How = How.Name, Using = "when")]
        public IWebElement WhenField { get; set; }

        //поле тип транспорта
        [FindsBy(How = How.CssSelector, Using = "input[value=\"bus\"]")]
        public IWebElement Bus { get; set; }

        //кнопка найти
        [FindsBy(How = How.CssSelector, Using = "button[type=\"submit\"][data-block=\"y-button\"]")]
        public IWebElement FindButton { get; set; }

        //заголовок поиска
        [FindsBy(How = How.XPath, Using = "//*[@id=\"root\"]/div/main/div[@class=\"SearchLayout\"]/div[@class=\"SearchLayout__content\"]/div[@class=\"SearchPage\"]/div/div[@class=\"SearchHeader\"]/header[@class=\"SearchTitle\"]/h1")]
        public IWebElement SearchHeaderSpan { get; set; }

        //заголовок поиска дата
        [FindsBy(How = How.XPath, Using = "//*[@id=\"root\"]/div/main/div[@class=\"SearchLayout\"]/div[@class=\"SearchLayout__content\"]/div[@class=\"SearchPage\"]/div/div[@class=\"SearchHeader\"]/header[@class=\"SearchTitle\"]/span/span[2]")]
        public IWebElement SearchHeaderDateSpan { get; set; }

        //      
        [FindsBy(How = How.CssSelector, Using = "ul[class=\"SearchSegments\"]")]
        public IWebElement ResultUl { get; set; }

        //сохраняем данные о рейсе
        //возвращаем элемент, при клике на который происходит переход на детальную страницу с данныи о найденно рейсе
        public IWebElement SaveRaceInfo(IWebElement CurrentElement) {
            //сохраняем значения для найденного рейса
            //перевозчик
            BusData.carrier = CurrentElement.FindElement(By.XPath(".//a[@class=\"Link SearchSegment__company\"]")).Text;
            //время отправления
            BusData.StTime = CurrentElement.FindElement(By.XPath(".//div[@class=\"SearchSegment__time Time_important\"][1]/span")).Text;
            //откуда
            BusData.from = CurrentElement.FindElement(By.XPath(".//div[@class=\"SearchSegment__stationHolder\"][1]/a")).Text;
            //куда
            BusData.to = CurrentElement.FindElement(By.XPath(".//div[@class=\"SearchSegment__stationHolder\"][2]/a")).Text;
            //время прибытия
            BusData.EndTime = CurrentElement.FindElement(By.XPath(".//div[@class=\"SearchSegment__time\"]/span")).Text;
            //время в пути
            BusData.timeIn = CurrentElement.FindElement(By.XPath(".//div[@class=\"SearchSegment__duration\"]/span")).Text;

            string pattern = @"\d+[.,]?[0-9]{2}?";
            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            //достаем из строки цену 
            Match price = rgx.Match(CurrentElement.FindElement(By.XPath(".//span[@class=\"Price TariffsListItem__price\"]")).Text);
            // сохраняем цену в рублях
            BusData.rubPrice = Convert.ToDouble(price.Value, System.Globalization.CultureInfo.InvariantCulture);

            //меняем валюту
            PropertiesCollection.driver.FindElement(By.XPath("//*[@id=\"root\"]/div/main/div/div[1]/div[1]/div/ul[1]/li[4]/div")).Click();
            PropertiesCollection.driver.FindElement(By.XPath("//div[@data-value=\"USD\"]")).Click();

            price = rgx.Match(CurrentElement.FindElement(By.XPath(".//span[@class=\"Price TariffsListItem__price\"]")).Text);

            //цена в долларах
            BusData.usdPrice = Convert.ToDouble(price.Value, System.Globalization.CultureInfo.InvariantCulture);

            //время в пути
            BusData.timeIn = CurrentElement.FindElement(By.XPath(".//div[@class=\"SearchSegment__duration\"]/span")).Text;


            BusData.fromCity = FromField.GetAttribute("value");
            BusData.toCity = ToField.GetAttribute("value");
            //пишем информацию о рейсе в консоль
            Console.WriteLine("Рейс  из " + BusData.from + " в " + BusData.to + " время отправления: " + BusData.StTime.ToString() + "; цнеа " + BusData.rubPrice + "руб. или " + BusData.usdPrice + " $;");
            return CurrentElement.FindElement(By.XPath(".//a[@class=\"Link SearchSegment__link\"]"));

        }

        //находим рейс не раньше afterHours и уходящий с автовокзала busStation
        public IWebElement TestSerchResults(int afterHours,string busStation) {

            //список результатов поиска
            IReadOnlyCollection<IWebElement> SearchSegments = ResultUl.FindElements(By.CssSelector("div[class=\"SearchSegment\"]"));
            for (int i = 0; i < SearchSegments.Count; i++)
            {
                IWebElement CurrentElement = SearchSegments.ElementAt(i);

                if (
                    Int32.Parse(CurrentElement.FindElement(By.XPath(".//div[@class=\"SearchSegment__time Time_important\"]/span")).Text.Substring(0, 2)) >= afterHours
                    && CurrentElement.FindElement(By.XPath(".//div[@class=\"SearchSegment__stationHolder\"][1]/a")).Text.Contains(busStation)
                    )
                {

                    return CurrentElement;
 
                }

            }
            Console.WriteLine("Подходящих рейсов не найдено");
            return null;

            //Assert.IsFalse(SearchHeaderSpan.Text.Contains(from), "from");
        }

        public YaDetailpageObject GoToDetailPage(IWebElement elementToClick) {
            if (elementToClick != null)
            {
                elementToClick.Click();
            }
            
            return new YaDetailpageObject();
        }

        public void EnterSearchParams(string from, string to, DateTime when,TransportType trType = TransportType.автобус)
        {
            //устанавливаем, что ищем автобус
            if (trType == TransportType.автобус) {
                Bus.FindElement(By.XPath("ancestor::label")).Click();
            }

            //откуда едем
            FromField.SendKeys(Keys.Control + "a" + Keys.Delete + from);
            //куда едем
            ToField.SendKeys(to);
            
            //когда едем
            WhenField.SendKeys(when.ToString("d"));
            //запускаем поиск
            FindButton.Click();

            //проверяем что заголовок соответствует тому что ищем
            if (
                !SearchHeaderSpan.Text.Contains(from) ||
                !SearchHeaderSpan.Text.Contains(to)||
                !SearchHeaderSpan.Text.Contains(trType.ToString()) ||
                !SearchHeaderDateSpan.Text.Contains(when.Day.ToString())
                )
            {
                throw new Exception("Заголовок поиска не соответствует переданным параметрам");
            }

        }

    }
}
