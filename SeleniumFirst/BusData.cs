﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumFirst
{
    class BusData
    {
        public static string carrier { get; set; }
        public static string from { get; set; }
        public static string to { get; set; }
        public static string timeIn { get; set; }
        public static string StTime { get; set; }
        public static string EndTime { get; set; }
        public static double rubPrice { get; set; }
        public static double usdPrice { get; set; }
        public static string fromCity { get; set; }
        public static string toCity { get; set; }

    }
}
