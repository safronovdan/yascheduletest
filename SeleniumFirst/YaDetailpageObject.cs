﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SeleniumFirst
{
    class YaDetailpageObject
    {
        public YaDetailpageObject()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        //заголовок тип транспорта
        [FindsBy(How = How.XPath, Using = "//h1[@class=\"b-page-title__title\"]/span[1]")]
        public IWebElement SearchHeaderTransportType { get; set; }

        //заголовок откуда
        [FindsBy(How = How.XPath, Using = "//h1[@class=\"b-page-title__title\"]/span[2]")]
        public IWebElement SearchHeaderFrom { get; set; }

        //заголовок куда
        [FindsBy(How = How.XPath, Using = "//h1[@class=\"b-page-title__title\"]/span[3]")]
        public IWebElement SearchHeaderTo { get; set; }

        //заголовок перевозчик
        [FindsBy(How = How.XPath, Using = "//div[@class=\"b-page-title__text\"]")]
        public IWebElement SearchHeaderTransporter { get; set; }

        //таблица откуда
        [FindsBy(How = How.XPath, Using = "/html/body/div[1]/div/div/div/table/tbody/tr[2]/td[2]/div/div/table/tbody/tr[2]/td[1]/div[2]/a")]
        public IWebElement TableFrom { get; set; }

        //таблица куда
        [FindsBy(How = How.XPath, Using = "/html/body/div[1]/div/div/div/table/tbody/tr[2]/td[2]/div/div/table/tbody/tr[3]/td[1]/div[2]/a")]
        public IWebElement TableTo { get; set; }

        //таблица время отправления
        [FindsBy(How = How.XPath, Using = "/html/body/div[1]/div/div/div/table/tbody/tr[2]/td[2]/div/div/table/tbody/tr[2]/td[2]/span/span/strong")]
        public IWebElement TableWhenStart { get; set; }

        //таблица время прибытия
        [FindsBy(How = How.XPath, Using = "/html/body/div[1]/div/div/div/table/tbody/tr[2]/td[2]/div/div/table/tbody/tr[3]/td[2]/span/span/strong")]
        public IWebElement TableWhenEnd { get; set; }

        //таблица время в пути
        [FindsBy(How = How.XPath, Using = "/html/body/div[1]/div/div/div/table/tbody/tr[2]/td[2]/div/div/table/tbody/tr[3]/td[3]/div")]
        public IWebElement TableDuration { get; set; }



        public void TestRaceInfo()
        {
            //проверяем заголовок таблицы
 
            Assert.IsTrue(SearchHeaderFrom.Text.Contains(BusData.fromCity), "заголовок: Город отъезда не соответствует требуемому");
            
            Assert.IsTrue(SearchHeaderTo.Text.Contains(BusData.toCity), "заголовок: Город приезда не соответствует требуемому");
            Assert.IsTrue(SearchHeaderTransporter.Text.Contains(BusData.carrier), "Заголовок: Перевозчик не соответствует требуемому");
            //проверяем данные таблицы

            
            Assert.IsTrue(TableFrom.Text.Contains(BusData.fromCity), "таблица: город отправления не соответствует требуемому");

            Assert.IsTrue(TableTo.Text.Contains(BusData.toCity), "таблица: город прибытия не соответствует требуемому");
            Assert.IsTrue(TableWhenStart.Text.Contains(BusData.StTime), "таблица: время отправления не соответствует требуемому");
            Assert.IsTrue(TableWhenEnd.Text.Contains(BusData.EndTime), "таблица: время прибытия не соответствует требуемому");
            
            Assert.IsTrue(TableDuration.Text.Contains(BusData.timeIn), "таблица: время в пути не соответствует требуемому");

        }








    }
}
