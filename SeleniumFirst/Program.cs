﻿
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumFirst
{
    class Program
    {
       

        static void Main(string[] args)
        {
      
        }

        [SetUp]
        public void Initialize()
        {
            //драйвер хрома 
            // стандартный устанавливаемый через NuGet - возможно не будет работать
            // я использовал драйвер версии вроде бы 2.9 
            PropertiesCollection.driver = new ChromeDriver(@"C:\chromedriver_win32\");
            //PropertiesCollection.driver = new ChromeDriver();
            PropertiesCollection.driver.Navigate().GoToUrl("https://www.yandex.ru/");
            Console.WriteLine("open url");
        }

        // возвращает ближайшую субботу
        public DateTime GetNearestSaturday()
        {  
            DateTime today = DateTime.Now;
            int diff = (int)DayOfWeek.Saturday - ((int)today.DayOfWeek);
            return (today.AddDays(diff));
        }

        // или можно еще вот так
        // т.к будет максимум 7 итераций 
        public DateTime GetNearestSaturday1()
        {
            DateTime today = DateTime.Now;
            while (today.DayOfWeek!= DayOfWeek.Saturday) {
                today = today.AddDays(1);
            }
            return (today);
        }



        [Test]
        public void ExecuteTest()
        {
            //главная страница яндекса
            YaMainPageObject yaMainPage = new YaMainPageObject();
            //страница расписаний
            YaSchedulePageObject yaSchedulePage = yaMainPage.GoToSchedule();
            //добавляем параметры поиска и проверяем заголовок таблицы
            yaSchedulePage.EnterSearchParams("Екатеринбург", "Каменск-Уральский", GetNearestSaturday(), TransportType.автобус);
            //ищем нужный рейс 
            IWebElement raceEl = yaSchedulePage.TestSerchResults(12, "Северный");
            if (raceEl!=null)
            {
                //записываем информацию  и переходим на детальную страницу
                //детальная страница
                YaDetailpageObject yaDetailPage = yaSchedulePage.GoToDetailPage(yaSchedulePage.SaveRaceInfo(raceEl));
                // проверяем, что данные на детальной странице соответствуют сохраненным данным о рейсе
                yaDetailPage.TestRaceInfo();
            }
            

            // ждем немного, чтобы окно не сразу закрывалось и было видно результат
            System.Threading.Thread.Sleep(2500);
            Console.WriteLine("execute test");
        }

        [TearDown]
        public void CleanUp()
        {
            PropertiesCollection.driver.Close();
            Console.WriteLine("close");
        }
    }
}
