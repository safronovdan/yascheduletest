﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumFirst
{
    class YaMainPageObject
    {

        public YaMainPageObject()
        {
            PageFactory.InitElements(PropertiesCollection.driver, this);
        }

        // основоное поле поиска
        [FindsBy(How = How.Name,Using ="text")]
        public IWebElement SearchFieldName { get; set; }

        // кнопка еще
        [FindsBy(How = How.ClassName, Using = "home-tabs__more")]
        public IWebElement MoreTab { get; set; }

        //ссылка на страницу расписания
        [FindsBy(How = How.CssSelector, Using = "a[data-id=\"rasp\"]")]
        public IWebElement RaspLink { get; set; }

        //переход на страницу расписания
        public YaSchedulePageObject GoToSchedule()
        {
            MoreTab.Click();
            RaspLink.Click();
            return new YaSchedulePageObject();
        }
    }
}
